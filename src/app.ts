import express from "express";
import log from "@ajar/marker";
import morgan from "morgan";
import apiRouter from "./api.js";
import webRouter from "./web.js";

const { PORT = 3030, HOST= "localhost" } = process.env;

const app = express();

const logger = (req:express.Request, res:express.Response,next:express.NextFunction)=> {
    log.info(`my host:${ req.host }, myPort: ${ PORT}`)
    next();
}

app.use( logger )
app.use(morgan("dev"));

app.use('/api', apiRouter);
app.use('/',webRouter);

app.listen(Number(PORT), HOST, () => {
    log.magenta(`🌎  listening on`, `http://${HOST}:${PORT}`);
  });
  