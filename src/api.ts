import express,{RequestHandler} from "express";
import log from "@ajar/marker";

const router = express.Router();

const payment:RequestHandler = (req,res,next)=>{
    log.magenta("payment process start now");
    res.redirect("/login");
    // next();
}

router.get('/', async (req,res)=>{
    res.json({msg:"main api route"})
})

router.get("/pop", (req,res)=>{
    res.json({msg:"pop api route"})

})

router.get('/payed', payment,(req,res)=>{
    res.json({msg:"payed finish"})
})

export default router;